//
//  main.cpp
//

#include <iostream>
#include "SkipList.h"

int main(int argc, const char * argv[])
{
    SkipList<int> myList;
    
    myList.printList();
    
    std::cout << std::endl;
    std::cout << "List Insert Tests" << std::endl;    
    myList.insert(4);
    myList.insert(1);
    myList.insert(2);
    myList.insert(1);
	myList.insert(19);
	myList.insert(7);
	myList.insert(3);
	myList.insert(8);
	myList.insert(13);
	myList.insert(9);
	myList.insert(17);
	myList.insert(3);
	myList.insert(12);
	myList.insert(13);
    myList.printList();
    
    std::cout << std::endl;
    std::cout << "List Find Tests" << std::endl;
	std::cout << myList.find(4) <<std::endl;
	std::cout << myList.find(20) <<std::endl;
	std::cout << myList.find(13) <<std::endl;
	std::cout << myList.find(19) <<std::endl;
    
    myList.find(1);
    myList.find(5);
	myList.find(13);
	myList.find(7);
	myList.find(15);
	myList.find(19);
   
    std::cout << std::endl;
    std::cout << "List Remove Tests" << std::endl;
    myList.remove(2);
    myList.remove(7);
    myList.remove(3);
	myList.remove(13);
	myList.remove(7);
	myList.remove(3);
	myList.remove(19);
	myList.remove(17);
    myList.printList();
    
    std::cout << std::endl;
    std::cout << "List Empty Tests" << std::endl;
    myList.makeEmpty();
    myList.printList();

    return 0;
}
