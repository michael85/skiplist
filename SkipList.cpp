//
//  SkipList.cpp
//

#include "SkipList.h"
#include "Flags.h"

/* **************************************************************** */

#if CONSTRUCTOR || ALL
template <class T>
SkipList<T>::SkipList(){
	head = new Node<T>( T(), maxHeight );
	head->height = 1;
}
#endif

/* **************************************************************** */

#if DESTRUCTOR || ALL
template <class T>
SkipList<T>::~SkipList(){
/*
	while(head->next != NULL){
		delete head->next;
	}
*/
	makeEmpty();
	delete head;
}
#endif

/* **************************************************************** */

#if FIND || ALL
template <class T>
bool SkipList<T>::find(const T & x) const{	

	Node<T> *findNode = head;
	for(int i = findNode->height - 1; i >= 0; i--){
		//for( ; findNode->next[i] != NULL; findNode = findNode->next[i]){
		while(findNode->next[i] != NULL){
			
			if(findNode->next[i]->data > x){
				//return false;				
				break;
			}
			if(findNode->next[i]->data == x){
				return true;
			}
			findNode = findNode->next[i];
		}
	}
	return false;
}
#endif

/* **************************************************************** */

#if INSERT || ALL
template <class T>
void SkipList<T>::insert(const T & x){
	int newHeight = randomLevel();	
	Node<T> *newNode = new Node<T>(x, newHeight);
	Node<T> *update[maxHeight + 1];
	Node<T> *cur = head;
	
	if(!find(x)){
		for(int i = maxHeight-1; i >= 0; i--){
			while(cur->next[i] != NULL && cur->next[i]->data < newNode->data){
				cur = cur->next[i];
			}
			update[i] = cur;
		}
		cur = cur->next[0];
		for(int i = 0; i < newHeight; i++){
			newNode->next[i] = update[i]->next[i];
			update[i]->next[i] = newNode;
		}	
	}
}
#endif

/* **************************************************************** */

#if REMOVE || ALL
template <class T>
void SkipList<T>::remove(const T & x){
	Node<T> *update[maxHeight + 1];
	Node<T> *cur = head;
	if(find(x)){
		for(int i = maxHeight-1; i >= 0; i--){
			while(cur->next[i] != NULL && cur->next[i]->data < x){		
				cur = cur->next[i];
			}
			update[i] = cur;
		}
		cur = cur->next[0];
		if(cur->data == x){
			for(int i = 0; i <= cur->height; i++){
				if(update[i]->next[i] != cur){
					break;
				}
				update[i]->next[i] = cur->next[i];
			}
			delete cur;
		}
	}
}
#endif

/* **************************************************************** */

#if ISEMPTY || ALL
template <class T>
bool SkipList<T>::isEmpty() const{
	Node<T> *n = head;
	if(n->next[0] != NULL){
		return false;
	}
	return true;
}
#endif

/* **************************************************************** */

#if MAKEEMPTY || ALL
template <class T>
void SkipList<T>::makeEmpty(){
	Node<T> *n = head;
	Node<T> *m = head;

	while(n->next[0] != NULL){
		m = n;
		n = n->next[0];
		delete m;
	}
	head = new Node<T>( T(), maxHeight );
	head->height = 1;
}
#endif

/* **************************************************************** */

#if RANDOMLEVEL || ALL
template <class T>
int SkipList<T>::randomLevel(){
	int l = 1;
	while((getRandomNumber() < 0.5) && (l <= maxHeight)){
		l++;
	}
	return l;
}
#endif

/* **************************************************************** */
// Do NOT modify anything below this line
/* **************************************************************** */

#ifndef BUILD_LIB
// random number generator
template <class T>
double SkipList<T>::getRandomNumber()
{
    static int i = 0;
    static double a[10] = {0.32945,0.85923,0.12982,0.16250,0.56537,0.64072,0.27597,0.83957,0.75531,0.22502};
    
    return a[(i++)%10];
}


// printList() method
template <class T>
void SkipList<T>::printList()
{
    Node<T> *n = head;
    
    if (isEmpty())
    {
        std::cout << "Empty List" << std::endl;
    } else {
        while(n->next[0] != NULL)
        {
            n = n->next[0];
            // Print node data
            std::cout << "Node " << n->data << " height " << n->height << ": ";
            
            // Print levels
            for (int i=(n->height - 1); i >= 0; i--) {
                std::cout << "Level " << i << " -> ";
                if (n->next[i] != NULL) {
                    std::cout << n->next[i]->data;
                } else {
                    std::cout << "NULL";
                }
                std::cout << ",";
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
    }
}
#endif

template class SkipList<int>;


